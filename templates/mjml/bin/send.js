var API_KEY = 'key-1124fa5cc6bff886673af343a1dad16e';
var DOMAIN = 'mg.matiere-vive.ch';
const to = ['yassin@matiere-vive.ch'];
var mailgun = require('mailgun-js')({apiKey: API_KEY, domain: DOMAIN});
var fs = require('fs');
var pkjson = require('../package.json');

const commandLineArgs = require('command-line-args')
const options = commandLineArgs([
  { name: 'to', alias:'t', type: String, multiple: true }
]);

fs.readFile('public/index.html', 'utf8', function(err, contents) {
  const data = {
    from: `${pkjson.author.name} <postmaster@mg.matiere-vive.ch>`,
    "h:Reply-To": pkjson.author.email,
    to: (options.to || to).toString(),
    subject: `${pkjson.name} v${pkjson.version}`,
    html: contents
  };
  
  mailgun.messages().send(data, (error, body) => {
    console.log(body);
  });
});

