#!/usr/bin/env node
const inquirer = require('inquirer');
const fs = require('fs');

const CHOICES = fs.readdirSync(`${__dirname}/templates`);

function transform(answer) {
  return answer.by
}
function createMatch(key, value) {
  return { "search": `:>${key}<:`, by: value};
}

const QUESTIONS = [
  {
    name: 'projectChoice',
    type: 'list',
    message: 'What project template would you like to generate?',
    choices: CHOICES
  },
  // variables for mjml
  {
    name: 'mjmlTitle',
    type: 'input',
    message: 'Title of this mail',
    when: ({projectChoice}) => projectChoice==='mjml'
  },
  {
    name: 'mjmlPadding',
    type: 'input',
    message: 'Default padding',
    default: "20",
    validate: (input) => /\d/.test(input) ? true : 'Please enter a number',
    when: ({projectChoice}) => projectChoice==='mjml'
  },
  {
    name: 'projectName',
    type: 'input',
    message: 'Project name:',
    validate: function (input) {
      if (/^([A-Za-z\-\_\d])+$/.test(input)) return true;
      else return 'Project name may only include letters, numbers, underscores and hashes.';
    }
  }
];

const CURR_DIR = process.cwd();

inquirer.prompt(QUESTIONS)
  .then(answer => {
    const { projectChoice, ...replaceVars } = answer;
    const { projectName } = answer;

    const templatePath = `${__dirname}/templates/${projectChoice}`;
  
    fs.mkdirSync(`${CURR_DIR}/${projectName}`, {recursive:true});

    createDirectoryContents(templatePath, projectName, replaceVars);
  }
);


function createDirectoryContents (templatePath, newProjectPath, replaceVars={}) {
  const filesToCreate = fs.readdirSync(templatePath);

  filesToCreate.forEach(file => {
    const origFilePath = `${templatePath}/${file}`;
    
    // get stats about the current file
    const stats = fs.statSync(origFilePath);

    if (stats.isFile()) {
      var contents = fs.readFileSync(origFilePath, 'utf8');
      // replace input vars
      Object.keys(replaceVars)
        .forEach(key=> contents=contents.replace(`:>${key}<:`,replaceVars[key]));
      const writePath = `${CURR_DIR}/${newProjectPath}/${file}`;
      fs.writeFileSync(writePath, contents, 'utf8');
    } else if (stats.isDirectory()) {
      fs.mkdirSync(`${CURR_DIR}/${newProjectPath}/${file}`);
      // recursive call
      createDirectoryContents(`${templatePath}/${file}`, `${newProjectPath}/${file}`, replaceVars);
    }
  });
}